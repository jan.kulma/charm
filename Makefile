start:
	docker-compose up

lint-fix:
	docker-compose exec nuxt npm run lint -- --fix

npm-install:
	docker-compose exec nuxt npm install --save $(p)

run:
	docker-compose exec nuxt $(c)
