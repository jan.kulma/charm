const LABEL_1 = 'Life Goal'
const LABEL_2 = 'Milestone'
const LABEL_3 = 'Project'
const LABEL_4 = 'Task'

export default {
  methods: {
    getChildLabel (parent) {
      if (parent === undefined || parent === null) {
        return LABEL_1
      }
      switch (parent.label) {
        case LABEL_1:
          return LABEL_2
        case LABEL_2:
          return LABEL_3
        case LABEL_3:
          return LABEL_4
      }
    },

    getLevel (label) {
      switch (label) {
        case LABEL_1:
          return 0
        case LABEL_2:
          return 1
        case LABEL_3:
          return 2
        case LABEL_4:
          return 3
      }
    }
  }
}
