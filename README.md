## charm

Serverless Nuxt.js + Firebase app. Goal oriented task manager.

This is my personal project where I learn Vue and Firebase ecosystem.

ATM its in MVP stage, and following are planned:
* Rewrite in TypeScript
* Upgrade to Vue 3 (will need to wait for Nuxt.js)
* Continue adding features


https://charm-498c1.web.app/

![](screenshot.png)


