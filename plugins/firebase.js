import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

if (!firebase.apps.length) {
  // Yes, it's safe to have these keys public https://firebase.google.com/docs/projects/learn-more#config-files-objects
  const config = {
    apiKey: 'AIzaSyB26XFY1wOUi8-fGnxw1aqRHdAUPZJk5sM',
    authDomain: 'charm-498c1.firebaseapp.com',
    databaseURL: 'https://charm-498c1.firebaseio.com',
    projectId: 'charm-498c1',
    storageBucket: 'charm-498c1.appspot.com',
    messagingSenderId: '794640931922',
    appId: '1:794640931922:web:f312052726b96fdd2c938b'
  }

  firebase.initializeApp(config)
}
const authProvider = new firebase.auth.GoogleAuthProvider()
const fireDb = firebase.firestore()
const fieldValue = firebase.firestore.FieldValue
export { fireDb, fieldValue, authProvider, firebase }
