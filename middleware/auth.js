export default function (context) {
  if (context.route.name !== 'index') {
    if (!context.store.getters.isAuthenticated) {
      context.redirect('/')
    }
  }
}
