import _ from 'underscore'

export default class Goal {
  id
  label
  name
  description
  parent
  children

  constructor (
    id = '',
    label = '',
    name = '',
    description = '',
    parent = false,
    children = []
  ) {
    this.id = id
    this.label = label
    this.name = name
    this.description = description
    this.parent = parent
    this.children = children
  }

  validate () {
    return this.name !== '' && this.description !== ''
  }

  static createFromFirebaseDoc (doc) {
    const parent = doc.get('parent') ? doc.get('parent').id : doc.get('parent')
    const children = _.map(doc.get('children'), (child) => {
      return child.id
    })

    return new Goal(
      doc.id,
      doc.get('label'),
      doc.get('name'),
      doc.get('description'),
      parent,
      children
    )
  }
}
