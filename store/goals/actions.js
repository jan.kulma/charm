/* eslint-disable */

import _ from 'underscore'
import { fieldValue, fireDb } from '~/plugins/firebase'
import GoalModel from '~/models/Goal'

export default {

  /*
   * Firebase actions
   */

  async $_get (context, payload) {
    return await fireDb
      .collection(payload.path)
      .where('userId', '==', context.getters.getUserId)
      .get()
  },

  async $_put (context, payload) {
    payload.fields.userId = context.getters.getUserId
    return await fireDb
      .doc(payload.path)
      .set(payload.fields, { merge: true })
  },

  async $_post (context, payload) {
    payload.fields.userId = context.getters.getUserId
    return await fireDb
      .collection(payload.path)
      .add(payload.fields)
  },

  async $_delete (context, payload) {
    return await fireDb
      .doc(payload.path)
      .delete()
  },

  /*
   * Firebase handlers
   */

  async get (context) {
    let querySnapshot
    const list = []
    try {
      querySnapshot = await context.dispatch('$_get', { path: 'goals' })
    } catch (e) {
      // todo error handling
      console.log(e)
    }

    querySnapshot.forEach((doc) => {
      const goal = GoalModel.createFromFirebaseDoc(doc)
      list.push(goal)
    })

    context.commit('SET_GOALS', list)
  },

  async createGoal (context, goal) {
    let doc
    try {
      doc = await context.dispatch('$_post', {
        path: 'goals',
        // todo use Goal class and get the fields smarter
        fields: { label: goal.label, name: goal.name, description: goal.description, parent: false, children: [] }
      })
    } catch (e) {
      console.log(e)
      return
    }

    goal.id = doc.id

    context.dispatch('setSelectedGoalId', goal.id)

    context.commit('CREATE_GOAL', goal)

    return goal.id
  },

  async createSubGoal (context, goal) {
    const parentReference = fireDb.doc('goals/' + goal.parent)
    let doc
    try {
      // todo use Goal class and get the fields smarter
      doc = await context.dispatch('$_post', {
        path: 'goals',
        fields: {
          label: goal.label,
          name: goal.name,
          description: goal.description,
          parent: parentReference,
          children: []
        }
      })
    } catch (e) {
      console.log(e)
      return
    }

    goal.id = doc.id

    context.dispatch('setSelectedGoalId', goal.id)

    context.commit('CREATE_GOAL', goal)

    // update parents children
    try {
      await context.dispatch('$_put', {
        path: `goals/${goal.parent}`,
        fields: {
          children: fieldValue.arrayUnion(fireDb.doc('goals/' + doc.id))
        }
      })
    } catch (e) {
      console.log(e)
      return
    }

    const parent = context.getters.getGoalById(goal.parent)
    context.commit('ADD_CHILD_TO_PARENT_GOAL', { parent, child: goal.id })
  },

  async updateGoal (context, goal) {
    try {
      await context.dispatch('$_put', {
        path: `goals/${goal.id}`,
        fields: { name: goal.name, description: goal.description }
      })
    } catch (e) {
      console.log(e)
    }

    context.commit('UPDATE_GOAL', goal)
  },

  async deleteGoal (context, goal) {
    _.each(goal.children, async (child) => {
      await context.dispatch('deleteGoal', child)
    })

    await context.dispatch('$_delete', { path: `goals/${goal.id}` })
    context.commit('REMOVE_GOAL', goal.id)
  },

  setSelectedGoalId (context, goalId) {
    context.commit('SET_SELECTED_GOAL_ID', goalId)
  }
}
