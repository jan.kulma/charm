import _ from 'underscore'

export const state = () => ({
  list: [],
  selectedGoalId: ''
})

export const mutations = {
  SET_GOALS (state, goals) {
    state.list = goals
  },
  REMOVE_GOAL (state, id) {
    state.list = state.list.filter((item) => {
      return item.id !== id
    })
  },
  SET_SELECTED_GOAL_ID (state, id) {
    state.selectedGoalId = id
  },
  CREATE_GOAL (state, goal) {
    state.list.push(goal)
  },
  UPDATE_GOAL (state, goal) {
    const goalToUpdate = state.list.find(item => item.id === goal.id)
    Object.assign(goalToUpdate, goal)
  },
  ADD_CHILD_TO_PARENT_GOAL (state, { parent, child }) {
    parent.children.push(child)
  }
}

export const getters = {
  getSelectedGoalId (state) {
    return state.selectedGoalId
  },
  getRootElements (state) {
    return state.list.filter((goal) => {
      return goal.parent === false
    })
  },
  getGoalById: state => (id) => {
    return state.list.find(goal => goal.id === id)
  },
  getUserId (state, getters, rootState) {
    return rootState.user.id
  },
  getGoalWithChildren: (state, getters) => (id) => {
    const getChildren = function (childrenIds) {
      const children = []

      _.each(childrenIds, (id) => {
        const child = getters.getGoalById(id)
        if (child) {
          children.push({
            id: child.id,
            label: child.label,
            name: child.name,
            description: child.description,
            parent: child.parent,
            children: getChildren(child.children)
          })
        }
      })

      return children
    }

    const goal = getters.getGoalById(id)

    const goalWithChildren = {
      id: goal.id,
      label: goal.label,
      name: goal.name,
      description: goal.description,
      parent: goal.parent,
      children: getChildren(goal.children)
    }

    return goalWithChildren
  }
}
