export const state = () => ({
  user: {
    id: null
  }
})

export const actions = {
  logIn (context, payload) {
    context.commit('SET_USER', {
      id: payload.uid
    })
  },
  logOut (context) {
    context.commit('SET_USER', {
      id: null
    })
  }
}

export const mutations = {
  SET_USER (state, data) {
    state.user = data
  }
}

export const getters = {
  isAuthenticated (state) {
    return Boolean(state.user.id)
  }
}
